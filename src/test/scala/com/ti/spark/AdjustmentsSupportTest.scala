package com.ti.spark
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.scalatest.FunSuite
import org.scalatest.Matchers._
class AdjustmentsSupportTest extends FunSuite {
  test("Should adjust data properly") {
    implicit  val spark = SparkSession
      .builder()
      .appName("SparkAdjustments")
      //.config("spark.driver.memory","2g")
      .master("local[*]")
      .getOrCreate()
    import spark.implicits._
    val adjustmentsSupport : AdjustmentsSupport = new SparkAdjustments()

    val data: List[MrsPraTemplate] = List(
      MrsPraTemplate("31/12/2018","AHM1", "ABC1","Credit Default Swap", 100),
      MrsPraTemplate("31/12/2018","AHM1", "ABC2","Interest Rate Swap", 200),
      MrsPraTemplate("31/12/2018","AHM1", "ABC3","Cash Flow", 300),
      MrsPraTemplate("31/12/2018","AHM1", "ABC4","Cash Flow", 400)
    )

    val dataDF :DataFrame = spark.sparkContext.parallelize(data).toDF()
    val dataAdjustment: List[MrsPraTemplateAdjustment] = List(
      MrsPraTemplateAdjustment("31/12/2018","AHM1", "ABC1","Credit Default Swap", 500,"jwilso12", "cbirt", "fix bad data", "Insert"),
        MrsPraTemplateAdjustment("31/12/2018","AHM1", "ABC2","Interest Rate Swap", 200,"cbirt", "jwilso12", "Bad Product", "Delete"),
          MrsPraTemplateAdjustment("31/12/2018","AHM1", "ABC2","Cash Flow", 200,"cbirt", "jwilso12", "Bad Product", "Insert")
    )
    val dataAdjustmentDF :DataFrame = spark.sparkContext.parallelize(dataAdjustment).toDF()


   val results : List[MrsPraTemplate] = adjustmentsSupport.applyAdjustments(dataDF,dataAdjustmentDF).collect().toList.map(row => MrsPraTemplate(row(0).asInstanceOf[String],
     row(1).asInstanceOf[String],row(2).asInstanceOf[String],row(3).asInstanceOf[String],row(4).asInstanceOf[Int]))
    results should contain theSameElementsAs (List(
      MrsPraTemplate("31/12/2018","AHM1", "ABC1","Credit Default Swap", 100),
      MrsPraTemplate("31/12/2018","AHM1", "ABC1","Credit Default Swap", 500), // added
      MrsPraTemplate("31/12/2018","AHM1", "ABC3","Cash Flow", 300),
      MrsPraTemplate("31/12/2018","AHM1", "ABC4","Cash Flow", 400),
      MrsPraTemplate("31/12/2018","AHM1", "ABC2","Cash Flow", 200) // replaced
    ))

  }


  class SparkAdjustments(implicit spark: SparkSession) extends AdjustmentsSupport {
  }
}

case class MrsPraTemplate(cobdate: String,book: String, scenario : String, rdspst: String, pnl : Int)
case class MrsPraTemplateAdjustment(cobdate: String,book: String, scenario : String, rdspst: String, pnl : Int, Creator: String, Approver: String, Comment: String, Type: String)
