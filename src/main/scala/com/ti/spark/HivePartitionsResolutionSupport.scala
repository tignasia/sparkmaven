package com.ti.spark


import org.apache.spark.sql.{Row, SQLContext}

class HivePartitionsResolutionSupport(val sqlContext: SQLContext)  {

  def getAllPartitionPaths(tableName: String): Set[Row] = {
    val partitionsDF = sqlContext.sql(s"SHOW PARTITIONS ${tableName}")
    partitionsDF.collect().toSet
  }
}