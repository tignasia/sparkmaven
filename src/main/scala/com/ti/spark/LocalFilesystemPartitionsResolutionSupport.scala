package com.ti.spark

import java.nio.file.{Files, Path, Paths}

import org.apache.commons.io.FileUtils

//import org.apache.commons.io.FileSystemUtils

import scala.collection.JavaConverters._

class LocalFilesystemPartitionsResolutionSupport extends PartitionsResolutionSupport[Path] {

  override def getAllPartitionPaths(rootPath: Path, partitioningColumns: List[String]): Set[Path] = {
    Files.walk(rootPath).iterator().asScala
      .filter(path => Files.isDirectory(path) && pathContainsAnyPartitionName(path.toString, partitioningColumns))
      .map(rootPath.relativize(_)).toSet
  }

  override def deletePartitions(rootPath: Path,
                                partitionLocations: Set[Path]): Unit = {
    partitionLocations.foreach(path => FileUtils.deleteQuietly(Paths.get(rootPath.toString, path.toString).toFile))
  }
}