package com.ti.spark

import java.io.File

import com.ti.spark.Shakespeare.spark
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
// Import all functions
import org.apache.spark.sql.functions._
// Import $ notation
import spark.implicits._
object Oscars extends App {

  val spark = SparkSession
    .builder()
    .appName("Oscars")
    .config("spark.driver.memory","2g")
    .master("local[*]")
    .getOrCreate()
  val sc = spark.sparkContext
  val sqlContext = spark.sqlContext
  // toDF ect
  import sqlContext.implicits._


  val schema = new StructType()
    .add(StructField("Index", IntegerType, true))
    .add(StructField("Year", IntegerType, true))
    .add(StructField("Age", IntegerType, true))
    .add(StructField("Name", StringType, true))
    .add(StructField("Movie", StringType, true))
//  "Index", "Year", "Age", "Name", "Movie"

  val oscarsDF = sqlContext.read
    .option("charset", "utf-8")
    .option("header", "true")
    .option("quote", "\"")
    .option("delimiter", ",")
    .option("inferSchema",false)
    .option("ignoreLeadingWhiteSpace",true)
    .option("ignoreTrailingWhiteSpace",true)
    .schema(schema)
    .csv("/home/tomasz/development/Spark_Hadoop/sparkmaven/src/main/resources/oscar_age_female.csv",
    "/home/tomasz/development/Spark_Hadoop/sparkmaven/src/main/resources/oscar_age_male.csv")
      .sort("Year")
      .drop("Index")

  oscarsDF.printSchema()

  oscarsDF.show(200)

//  val oscarsMod = oscarsDF.select("Year","Name","Movie")
//  oscarsMod.show(200)

  val avgAgePerYear =  oscarsDF.groupBy(oscarsDF("Year")).avg("Age")//.alias("AvgAge")
  //avgAgePerYear.sta
  avgAgePerYear.show(200)

  val avgAgePerYearFor50Ands60s =  oscarsDF.where($"Year" > 1950  &&  $"Year" < 1970).groupBy(oscarsDF("Year")).avg("Age").withColumn("AvgAge2",$"avg(Age)" *2)

  avgAgePerYearFor50Ands60s.show(200)

  oscarsDF.select(concat($"Name",lit(" "), $"Movie")).as("NameMovie").show(200)

  val aggregationsPerYear =  oscarsDF.groupBy(oscarsDF("Year")).agg(avg($"Age").as("AvgAge"),
    min("Age").as("MinAge"),max($"Age").as("MaxAge"),
    stddev($"Age").as("StdDev")).sort(desc("AvgAge"))
  aggregationsPerYear.show(200)

  // Find oocurence of string
  val goneWithWind = oscarsDF.filter(locate("Gone with the Wind",$"Movie") > 0)
  goneWithWind.show()

  oscarsDF.filter(lower($"Movie").contains("gone with the wind")).show()
  //oscarsDF.filter(sp lower($"Movie").isin("gone with the wind","mary poppins","american beauty")).show()

  // Show people with multiple oscars group by name having count > 1
  oscarsDF.groupBy($"Name").agg(count($"Name").alias("count")).filter(column("count") > 1).show()
  //oscarsDF.groupBy($"Name").agg(count($"Name").alias("count")).filter(column("count") > 1).map(row => regexp_extract($"Name","(.*) (.*)",1).as($"FirstName"))

  // UDFs
  // custom function/udf

  val capFuntion = (fullString: String)  => fullString.split(" ").map(_.capitalize).mkString(" ")
  val capitalizeWords = udf(capFuntion)
  spark.udf.register("capitalizeWords",capFuntion)


  val texts = List(Text(1,"ala ma kota"),Text(2,"tomek ma psa"))
  val textsDF = spark.createDataFrame(texts)
  textsDF.schema.printTreeString()
  // Call register udf name
  textsDF.select($"id",callUDF("capitalizeWords", $"text").as("text")).show(truncate = false)
  //oscarsDF.withColumn($"Name",callUDF())
  //textsDF.withColumn("text",$"")
  textsDF.createOrReplaceTempView("texts")
  // Call UDF in SQL
  sqlContext.sql("select id, capitalizeWords(text) from texts").show(truncate = false) // as SQL

  // call UDF directly
  textsDF.select($"id",capitalizeWords($"text").as("text")).show(truncate = false)


  case class Text(id : Int, text: String)
}
