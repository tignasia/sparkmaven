package com.ti.spark

import java.io.{BufferedWriter, File, FileWriter}

import org.apache.calcite.sql.SqlIntervalLiteral.IntervalValue
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}

object Shakespeare extends App {

  val spark = SparkSession
    .builder()
    .appName("Shakespeare")
    .config("spark.driver.memory","2g")
    .master("local[*]")
    .getOrCreate()
  import spark.implicits._
  val sc = spark.sparkContext
  val sqlContext = spark.sqlContext





//  val content = scala.io.Source.fromURL("http://www.gutenberg.org/files/100/100-0.txt").mkString
//
//  val file = new File("shakespeare.txt")
//  val bw = new BufferedWriter(new FileWriter(file))
//  bw.write(content)
//  bw.close()

  val file = new File("/home/tomasz/development/Spark_Hadoop/sparkmaven/src/main/resources/shakespeare.txt")
  val lines = scala.io.Source.fromFile(file,"UTF-8").getLines().filter(_.trim.length > 2).toList
  //val lines = content.split("\n").filter(_.trim.length > 2)
  val linesRdd = sc.parallelize(lines)
  val wordsRdd = linesRdd.flatMap(line => line.split("\\s+")).map {
    word =>
    if (isNotProperWordEnding(word))
      word.substring(0, word.length - 1)
    else
      word
  }.filter(word => word.length > 2).map(_.toLowerCase)

  private def isNotProperWordEnding(word: String) = {

    word.endsWith(".") || word.endsWith(",") || word.endsWith(":") || word.endsWith("?") || word.endsWith(";")
  }

  val wordPairsRDD = wordsRdd.map(word => (word, 1))
  val top500 = wordPairsRDD.reduceByKey(_ + _).map(_.swap).top(500)

  top500.foreach(println(_))

  val schema = new StructType()
    .add(StructField("count", IntegerType, true))
    .add(StructField("word", StringType, true))

  //spark.createDataFrame(wordPairsRDD,schema)


  //val df = sqlContext.read.text("/home/tomasz/development/Spark_Hadoop/sparkmaven/src/main/resources/shakespeare.txt")



}
