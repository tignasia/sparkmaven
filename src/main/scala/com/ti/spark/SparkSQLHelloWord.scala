package com.ti.spark

//import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.sql.functions._

case class Abo(id: Int, name: (String, String))

case class Loc(id: Int, city: String)

case class Listing(street: String, zip:Int, price:Int)

/**
  * Created by tomasz on 5/7/2017.
  */
object SparkSQLHelloWord extends App {


  val warehouseLocation = "file:///${system:user.dir}/spark-warehouse"
  // Using SparkSession instead of SparkConf and SparkContext !!!
  val spark = SparkSession
    .builder()
    .appName("SparkSQLHelloWord")
    .config("spark.sql.crossJoin.enabled", "true")
    .master("local[*]")

    .config("spark.sql.warehouse.dir", warehouseLocation)
    .getOrCreate()

  val sc = spark.sparkContext
  val sqlContext = spark.sqlContext


  // toDF and toDS not visible without it !!
  import spark.implicits._

  // ########################################################################################################
  // Data Frames API  !!!
  //#########################################################################################################


  val as = List(Abo(101, ("Ruetli", "AG")), Abo(102, ("Brelaz", "DemiTarif")), Abo(103, ("Gress", "DemiTarifVisa")), Abo(104, ("Schatten", "DemiTarif")))
  val abosDF = sc.parallelize(as).toDF()

  val ls  = List(Loc(100,"Basel"), Loc(101,"Bern"),Loc(101,"Thun"),Loc(102,"Lausanne"),Loc(102,"Geneve"),Loc(102,"Nyon"),Loc(103,"Zurich"),Loc(103,"St-Gallen"),Loc(103,"Chur"))

  val locationsDF = sc.parallelize(ls).toDF


  abosDF.show()
  locationsDF.show()

  val joinDF =  abosDF.join(locationsDF,abosDF("id") === locationsDF("id"))// join types `inner`, `outer`, `left_outer`, `right_outer`, `leftsemi`.

  //val joinDF2 =  abosDF.join(locationsDF,Seq("id")/*,"inner"*/) // single id column
  //val joinDF2 =  abosDF.join(locationsDF,$"id" === $"id")
  val joinDF2 =  abosDF.join(locationsDF,"id"/*,"inner"*/) // single id column
  val joinDF3 =  abosDF.join(locationsDF,Seq("id"),"left_outer") // single id column left outer
  val joinDF4 =  abosDF.join(locationsDF,Seq("id"),"right_outer") // single id column right  outer
  val joinDF5 =  abosDF.join(locationsDF,Seq("id"),"full_outer") // single id column full outer
  val joinDF6 =  abosDF.join(locationsDF,Seq("id"),"left_semi") // single id column left semi - only left table with rows having coresponing id in other table
  val joinDF7 =  abosDF.join(locationsDF,Seq("id"),"left_anti") // single id column left anti - only left table with rows noy having coresponing id in other table
  val joinDF8 =  abosDF.crossJoin(locationsDF) // cross join

  joinDF.show()
  joinDF2.show()
  joinDF3.show()
  joinDF4.show()
  joinDF5.show()
  joinDF6.show()
  joinDF7.show()
  joinDF8.show(200)



  abosDF.createOrReplaceTempView("ABOS")
  locationsDF.createOrReplaceTempView("LOCATIONS")
  val joinDFBySQL =  sqlContext.sql("select * from ABOS,LOCATIONS WHERE ABOS.ID = LOCATIONS.ID")

  joinDFBySQL.show()


  val leftOuterJoinDF =  abosDF.join(locationsDF,abosDF("id") === locationsDF("id"),"left_outer")
  leftOuterJoinDF.show()

  val listings = List(Listing("Paderewskiego",55120,370000),Listing("Trzebnicka",55120,295000),Listing("Chelmonskiego",63400,260000),Listing("Smigielskiego",63400,250000))

  val listingsDF = sc.parallelize(listings).toDF
  val avgPricesDF = listingsDF.groupBy($"zip").avg("price").cache()
  avgPricesDF.show()

  val avPrices:Array[Row] = avgPricesDF.collect() // collects rows !!!
  avPrices.head.schema.printTreeString()

  val avgPrcesAgain = avPrices.map { row => (row(0).asInstanceOf[Int],row(1).asInstanceOf[Double])}
  println(avgPrcesAgain)

  // Data Frames - Optimalization but not tpe safety !!!

 // DataFme = Daaset[Row]

  // ########################################################################################################
  // Datasets API !
  //#########################################################################################################

  val listingsDS:Dataset[Listing] = sc.parallelize(listings).toDS // its typed dataset !!!
  val avPrices2:Dataset[(Int,Double)] =  listingsDS.groupByKey(l => l.zip) // passing lambda like in RDD !!!
  .agg(avg($"price").as[Double]) // but using agg avg like DataFrame operators !
  // avg() returns Column but agg requries TypedColumn .as[Double] converts Column to TypedColumn[Double]

  avPrices2.show()
  // On Dtaasets both DataFrame and RDD operation can be used  !!!



  // eg. map
  val tripleDS:Dataset[(String,Int,Int)] = listingsDS.map(l => (l.street,l.zip,l.price))
//  listingsDS.coalesce()

  // reduceByKey doesn't exist in Dataset API - instedad

  // Approach 1 (mapGroups very slow - dos shuffling)
  val zipToStreets =  tripleDS.groupByKey(t => t._2).mapGroups((k,t) => (k,t.foldLeft("")((acc,p) => acc + "," + p._1)))
  zipToStreets.show()

  // Approach 2 much fater that map Groups
  val zipToStreets2 =  tripleDS.groupByKey(t => t._2).mapValues(t => t._1).reduceGroups((acc,str) => acc + "," + str)
  zipToStreets2.show()

  import org.apache.spark.sql.expressions.Aggregator

  // Approach 3 user-defined aggregations, which can be used in `Dataset`
  val strConcat:TypedColumn[(String,Int,Int),String] = new Aggregator[(String,Int,Int),String,String] {
    override def zero: String = ""

    override def reduce(b: String, a: (String, Int, Int)): String = b + "," + a._1

    override def merge(b1: String, b2: String): String = b1 + b2

    override def finish(reduction: String): String = reduction

    // Tungsten (off heap serialization) helpers - uses schema information, memory efficient and very fast
    // orders of maginitude faster than java serialzatio and 10x Kryo
    override def bufferEncoder: Encoder[String] = Encoders.STRING

    override def outputEncoder: Encoder[String] = Encoders.STRING
  }.toColumn

  // Catalyst cannot optimize lambda funtions/clousures
  val zipToStreets3 =  tripleDS.groupByKey(t => t._2).agg(strConcat.as[String])
  zipToStreets3.show()



}
