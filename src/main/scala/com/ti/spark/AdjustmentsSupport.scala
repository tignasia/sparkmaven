package com.ti.spark

import com.ti.spark.AdjustmentsSupport.{approverColumn, commentColumn, creatorColumn, deleteType, insertType, typeColumn}
import org.apache.spark.sql.{Column, DataFrame, SparkSession}
// Import all functions
import org.apache.spark.sql.functions._
// Import $ notation
//
object AdjustmentsSupport {
  val typeColumn = "Type"
  val creatorColumn = "Creator"
  val approverColumn = "Approver"
  val commentColumn = "Comment"
  val insertType = "Insert"
  val deleteType = "Delete"
}
trait AdjustmentsSupport {
  def applyAdjustments(dataDF: DataFrame, adjustmentsDF : DataFrame/*, cobDate: String*/)(implicit spark: SparkSession): DataFrame = {
    import spark.implicits._

   // val dataDFWithoutScenarioColumn = dataDF.drop("Scenario")

    val adjustedProjection: Column = when(adjustmentsDF("Type") === "Insert", "Y").otherwise("N").as("Adjusted")
    val activeProjection: Column = lit("Y").as("Adjusted")
    val creatorProjection: Column = when(adjustmentsDF(creatorColumn).isNotNull, adjustmentsDF(creatorColumn)).otherwise(lit(null)).as(creatorColumn) // ???
    val approverProjection: Column = when(adjustmentsDF(approverColumn).isNotNull, adjustmentsDF(approverColumn)).otherwise(lit(null)).as(approverColumn) // ???
    val commentProjection: Column = when(adjustmentsDF(commentColumn).isNotNull, adjustmentsDF(commentColumn)).otherwise(lit(null)).as(commentColumn) // ???

    // PROBABLY WRONG !!!!
   // val scenarioProjection: Column = when(adjustmentsDF("Scenario").isNotNull, lit("Adjusted") ).otherwise(dataDF("Scenario")).as("Scenario") // ???

    //val adjustmentsDFWithoutScenarioColumn = adjustmentsDF.drop("Scenario")

    // separate delete and insert
    val insertsDF =  /*adjustmentsDFWithoutScenarioColumn*/adjustmentsDF.select($"*"/*adjustmentsDF*//*adjustmentsDFWithoutScenarioColumn*/).where(adjustmentsDF(typeColumn) === insertType)
    val deleteDF =  /*adjustmentsDFWithoutScenarioColumn*/adjustmentsDF.select($"*"/*adjustmentsDF*//*adjustmentsDFWithoutScenarioColumn*/).where(adjustmentsDF(typeColumn) === deleteType)
      //

    // adjustmentsDF:  remove extra columns
    val insertsPureDf =  dropExtraAdjustmentColumns(insertsDF) // TODO  if there are 2 adjustments changing the same column only the latest should be used - there should be
    // some timestamp column added

    val deletePureDf =  dropExtraAdjustmentColumns(deleteDF).distinct() // depends on equals() on Row which compares all columns including nulls
    //
    //
    // TODO - use full column set to join just not to  ues all - dataDF.columns to do the join
    val dataDFsWithDroppedRows =  /*dataDFWithoutScenarioColumn*/dataDF.join(deletePureDf,/*dataDFWithoutScenarioColumn*/dataDF.columns,"left_anti") // single id column left anti - only left table with rows noy having coresponing id in other table
    //val finalDataDf = dataDFsWithDroppedRows.unionAll(insertsDF.select($"*", lit("")/*, current_timestamp.cast("long")*/))

    // TODO - use full column set to join just not to  ues all - dataDF.columns to do the join
    val processedDataDf = dataDFsWithDroppedRows.unionByName(insertsPureDf/*.select($"*")*/).distinct() //deduplication ???
    processedDataDf
    // TODO Scenario => Adjusted ??? DO we need it despite having extra Adjusted column ?
   // val finalDataDf = processedDataDf.withColumn(creatorColumn,creatorProjection)
    // TODO Add Adjusted (This have to be added based on existence in insertsDF is  and Active columns (default Y)
    //val finalDataDf = processedDataDf.select($"*",  creatorProjection,approverProjection,commentProjection, activeProjection, adjustedProjection)
    //finalDataDf
}

  //
  //  AdjustmentsSupport.applyAdjustments(dataDF: DataFrame, adjustmentsDF : DataFrame, cobDate: String) {
 //    // 2. adjustmentsDF : separate delete and insert (two dtataframes) // possibly it is
  //    // 3. adjustmentsDF : remove rows (filter on all columns)
  //    // 4. adjustmentsDF:  remove extra columns (type only?)
  //    // 5. dataDF: Add columns adjusted , creator, approver, comment
  //    // 6. dataDF: Remove Rows (filter())
  //    // 7. dataDF: add rows (unionAll https://stackoverflow.com/questions/36926856/spark-sql-how-to-append-new-row-to-dataframe-table-from-another-table?rq=1 or maybe rather join (all colums)
  //    https://forums.databricks.com/questions/6534/how-to-append-new-column-values-in-dataframe-behal.html producting extra clumns
  //    https://stackoverflow.com/questions/36132322/join-two-data-frames-select-all-columns-from-one-and-some-columns-from-the-othe
  //    https://stackoverflow.com/questions/38721218/joining-two-dataframes-in-spark-sql-and-selecting-columns-of-only-one (Semi-join)
  //  }
  // TODO Deal with dimenstamp column (modification)
  private def dropExtraAdjustmentColumns(insertsDF: DataFrame) = {
    insertsDF.drop(creatorColumn).drop(approverColumn).drop(commentColumn).drop(typeColumn)
  }

  private def dropExtraAdjustmentColumns(columns: Array[String]) = {
    columns.toList.filter(columnName => !columnName.equalsIgnoreCase(creatorColumn) && !columnName.equalsIgnoreCase(approverColumn)
    && !columnName.equalsIgnoreCase(commentColumn) && !columnName.equalsIgnoreCase(typeColumn))
//    insertsDF.drop(creatorColumn).drop(approverColumn).drop(commentColumn).drop(typeColumn)
  }

}