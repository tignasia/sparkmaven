package com.ti.spark

/**
  * Created by tomasz on 5/7/2017.
  */
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf


object SimpleApp extends App {
  // create Spark context with Spark configuration
//  val sc = new SparkContext(new SparkConf().setAppName("IQ Simple Spark App").setMaster("local[*]"))
//  val rdd = sc.parallelize(Seq(1,2,3,4,5))
//  println(rdd.sum())

  val partitioningCriteria = "cob_date, chunk_id"
  val stagingPartitioningColumns = partitioningCriteria.split(",").map(_.trim).toList
  val stagingPartitioningTypes : List[String] = List("string","int")

  val finalPartitioningColumns = "snapshot_load_time" +: stagingPartitioningColumns
  val finalPartitioningTypes : List[String] = List("string","int", "string")


  println (stagingPartitioningColumns.zip(stagingPartitioningTypes).map(pair => s"${pair._1} ${pair._2}" ).mkString(","))

  println (finalPartitioningColumns.zip(finalPartitioningTypes).map(pair => s"${pair._1} ${pair._2}" ).mkString(","))


}