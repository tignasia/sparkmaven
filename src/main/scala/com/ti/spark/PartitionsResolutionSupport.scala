package com.ti.spark

trait PartitionsResolutionSupport[P] {

  def getAllPartitionPaths(rootPath: P, partitioningColumns: List[String]): Set[P]

  def deletePartitions(rootPath: P, partitionLocations: Set[P])

  protected def pathContainsAnyPartitionName(path: String, partitionNames: List[String]) = partitionNames
    .forall(partition => path.contains(partition + "="))


}