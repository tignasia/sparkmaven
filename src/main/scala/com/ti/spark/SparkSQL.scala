package com.ti.spark

//import org.apache.spark.SparkConf
import java.io.File
import java.nio.file.{FileSystems, Path, Paths}
import java.text.SimpleDateFormat
import java.util.Date

import org.apache.commons.lang3.RandomStringUtils
import org.apache.spark.sql._
import org.apache.spark.sql.functions._

/**
  * Created by tomasz on 5/7/2017.
  */
object SparkSQL extends App {

  case class Person(ID: Int, name: String, age: Int, numFriends: Int)

  def mapper(line: String): Person = {
    val fields = line.split(',')
    val person: Person = Person(fields(0).toInt, fields(1), fields(2).toInt, fields(3).toInt)
    person
  }

  private val RANDOM_SUFFIX = RandomStringUtils.randomAlphanumeric(10).toUpperCase

  val hadoopHome = new File("windows_libs/2.3.0.0").getAbsolutePath
  val metastoreDb = new File("metastore_db").getAbsoluteFile
  val tempStagingLocationFile = new File("temp/temp_" + RANDOM_SUFFIX).getAbsoluteFile

  val finalDataLocation = new File("data").getAbsoluteFile

  System.setProperty("hadoop.home.dir", hadoopHome)
  val warehouseLocation = s"file:///${System.getProperty("user.dir")}/spark-warehouse"
  val scratchDirLocation = s"file:///${System.getProperty("user.dir")}/hive"
  val warehouseLocationFile = new File(warehouseLocation)
  val scratchDirLocationFile = new File(scratchDirLocation)

  createInDoesntExist(warehouseLocationFile)
  createInDoesntExist(scratchDirLocationFile)

  deleteIfExist(metastoreDb)
  deleteIfExist(finalDataLocation)
  val javaLibPath = System.getProperty("java.library.path")
  // C:\Users\M314424\development\Spark\tignasia-sparkmaven\windows_libs\2.3.0.0\lib
  //System.setProperty("java.library.path", javaLibPath +  ";" +  new File(hadoopHome,"lib").getAbsolutePath)


  // Using SparkSession instead of SparkConf and SparkContext !!!
  val spark = SparkSession
    .builder()
    .appName("SparkSQLHelloWord")
    .master("local[*]")
    .config("spark.sql.warehouse.dir", warehouseLocation)
    .config("hive.exec.scratchdir", scratchDirLocation)
    .config("hive.exec.dynamic.partition", "true")
    .config("hive.exec.dynamic.partition.mode", "nonstrict")
    .config("spark.network.timeout", "600s")
    .enableHiveSupport()
    .getOrCreate()

  //val partitioningCriteria = ???
  val partitioningCriteria = "cob_date, chunk_id"
  val partitioningColumns = partitioningCriteria.split(",").map(_.trim).toList
  val partitioningTypes: List[String] = List("string", "int")

  val finalPartitioningColumns = "snapshot_load_time" +: partitioningColumns
  val finalPartitioningTypes: List[String] = "string" +: partitioningTypes
  val sc = spark.sparkContext
  val sqlContext = spark.sqlContext

  try {

    val finalDF: DataFrame = createAndFillFinalHiveTable(sqlContext, "FINAL_TABLE", finalDataLocation, finalPartitioningColumns, finalPartitioningTypes)
    val newIngestionDF: DataFrame = createAndFillStagingHiveTable(sqlContext, "test", tempStagingLocationFile, partitioningColumns, partitioningTypes)


    try {


      val finalData = sqlContext.read.parquet(finalDataLocation.getAbsolutePath)
      println(s"FINAL DATA SCHEMA : ${finalData.schema}")

      val newTempData = sqlContext.read.parquet(tempStagingLocationFile.getAbsolutePath)
      println(s"TEMP STAGING DATA SCHEMA : ${newTempData.schema}")


      val hivePartitionsResolutionSupport = new HivePartitionsResolutionSupport(sqlContext)
      val tempPartitions = hivePartitionsResolutionSupport.getAllPartitionPaths("test")

      //val tempPartitions =  sqlContext.sql(s"SHOW PARTITIONS test").cache()

      println(s"TEMP STAGING PARTITIONS: ${tempPartitions}")

      val finalDataLocationPath = FileSystems.getDefault.getPath(finalDataLocation.getPath)


//      val partitionsResolutionSupport = new LocalFilesystemPartitionsResolutionSupport()
//      val finalPartitions: Set[Path] = partitionsResolutionSupport.getAllPartitionPaths(finalDataLocationPath, partitioningColumns)

      val finalPartitionNames: Set[String] = hivePartitionsResolutionSupport.getAllPartitionPaths("FINAL_TABLE").map(row => row.mkString)
      println(s"FINAL PARTITION NAMES (${finalPartitionNames.size}): ${finalPartitionNames}")

      val finalPartitions: Set[Path] = finalPartitionNames.map(Paths.get(finalDataLocationPath.toString,_)).toSet

      println(s"FINAL PARTITIONS (${finalPartitions.size}): ${finalPartitions}")

      val newTempPartitionNames = tempPartitions.map(row => row.mkString)
      println(s"NEW TEMP PARTITION NAMES (${newTempPartitionNames.size}): ${newTempPartitionNames}")


      val newTempPartitionNamesRDD = sc.parallelize(newTempPartitionNames.toSeq)
      val finalPartitionsRDD = sc.parallelize(finalPartitions.toSeq)

      val finalPartitionsNamesRDD = finalPartitionsRDD.map(path => path.subpath(1, 1 + partitioningColumns.size)).map(_.toString).map(partPath => partPath.replace('\\', '/') /* Required only on Windows */)
      val existingPartitionNamesRDD = newTempPartitionNamesRDD.intersection(finalPartitionsNamesRDD)

      val partitionNamesToRemoveRDD = newTempPartitionNamesRDD.intersection(existingPartitionNamesRDD)
      val finalPartitionsPairsRDD = finalPartitionsNamesRDD.zip(finalPartitionsRDD) // touples


      val partitionsToRemoveRDD /*: Set[Path]*/ = partitionNamesToRemoveRDD.flatMap(finalPartitionsPairsRDD.lookup(_)) // can be inefficent


      // TODO This could be parallized and processed on spark

      val finalPartitionsNames: Set[String] = finalPartitions.map(path => path.subpath(1, 1 + partitioningColumns.size)).map(_.toString).map(partPath => partPath.replace('\\', '/') /* Required only on Windows */)

      println(s"FINAL PARTITION NAMES (${finalPartitionsNames.size}): ${finalPartitionsNames}")

      val existingPartitionNames = newTempPartitionNames & finalPartitionsNames //newTempPartitionNames.intersect(finalPartitionsNames)

      println(s"EXISTING PARTITION NAMES (${existingPartitionNames.size}): ${existingPartitionNames}")

      val partitionNamesToRemove = newTempPartitionNames.intersect(existingPartitionNames)

      println(s"PARTITIONS TO REMOVE (${partitionNamesToRemove.size}): ${partitionNamesToRemove}")

      val finalPartitionsMap = finalPartitionsNames.zip(finalPartitions).toMap
      val partitionsToRemove: Set[Path] = partitionNamesToRemove.flatMap(finalPartitionsMap.get(_))
      println(s"PARTITIONS TO REMOVE USING SCALA COLECTIONS (${partitionsToRemove.size}): ${partitionsToRemove}")

      val partitionsToRemoveBySpark: Set[Path] = partitionsToRemoveRDD.collect().toSet
      println(s"PARTITIONS TO REMOVE USING SPARK RDDS (${partitionsToRemoveBySpark.size}): ${partitionsToRemoveBySpark}")






      // TODO move partitions from temp
      //  writer.mode( SaveMode.Overwrite/*"append"*/).option("path",finalDatasetLocation.getAbsolutePath).partitionBy(newPartitioningColumns.mkString(", ")).saveAsTable("FINAL_TABLE")


      createInDoesntExist(finalDataLocation)
      // write to final destination (repatritioned by using )
      val valuesWitAddedTimestamp = newIngestionDF.withColumn("snapshot_load_time", /*unix_timestamp*/
        /*asc*/ lit(new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSSS").format(new Date())))

      val writer = valuesWitAddedTimestamp.write.partitionBy(finalPartitioningColumns: _*)
      println(s"Writing parquet into final location ${finalDataLocation.getAbsolutePath}")
      writer.mode(SaveMode.Append /*"append"*/).parquet(finalDataLocation.getAbsolutePath)



      // TODO delete partitionsToRemove
      val partitionsResolutionSupport = new LocalFilesystemPartitionsResolutionSupport()
      partitionsResolutionSupport.deletePartitions(finalDataLocationPath, partitionsToRemove)

      //partitionsToRemove.foreach(path => Files./*deleteIfExists() */delete(path))

      // TODO ALTER TALE LOCATION replacing old partitions wit new
      //spark.sql("ALTER TABLE foo_test ADD IF NOT EXISTS PARTITION (datestamp=20180102)")
      /*      CREATE TABLE foo_test (`col0` STRING, `col1` STRING, `col2` STRING)
            PARTITIONED BY (`datestamp` STRING)
            stored as PARQUET
            location '/mnt/foo_test';*/

      //      sqlContext.sql(s"CREATE EXTERNAL TABLE IF NOT EXISTS FINAL_TABLE PARTITIONED BY (snapshot_load_time string, cob_date string, chunk_id int) STORED AS PARQUET LOCATION 'file:///${dir}'")
      //      sqlContext.sql("ALTER TABLE LOCAION")

      //spark.sql("ALTER TABLE foo_test ADD IF NOT EXISTS PARTITION (datestamp=20180102)")


    } finally {

    }

  } finally {
    // TODO DELETE STAGING TEMP DIR
    deleteIfExist(tempStagingLocationFile)
    sqlContext.sql("DROP TABLE IF EXISTS test PURGE")
    spark.stop()
  }

  private def createAndFillFinalHiveTable(sqlContext: SQLContext, tableName: String, loacationDir: File,
                                          partitioningCriteria: List[String], partitioningTypes: List[String]) = {

    val partitionedBy = partitioningCriteria.zip(partitioningTypes).map(pair => s"${pair._1} ${pair._2}").mkString(",")
    val createTable = s"CREATE TABLE IF NOT EXISTS ${tableName} (a string , b string) partitioned by (${partitionedBy})"

    sqlContext.sql(createTable)

/*    val valuesWitAddedTimestamp = newIngestionDF.withColumn("snapshot_load_time", /*unix_timestamp*/
      /*asc*/ lit(new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSSS").format(new Date())))*/

    val  snapshotLoadTime = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSSS").format(new Date())
    val existingResultsDF = sqlContext.sql(s"SELECT * FROM ${tableName} ")
    var results = existingResultsDF.collectAsList()
    if (results.size() == 0) {
      val insert1 = s"insert into ${tableName} values ('${snapshotLoadTime}', 'a' , 'b' , '2017-10-18',1)"
      val insert2 = s"insert into ${tableName} values ('${snapshotLoadTime}', 'd' , 'e', '2017-10-18',2)"
      val insert3 = s"insert into ${tableName} values ('${snapshotLoadTime}',  'x' , 'y', '2017-10-19',1)"
      val insert4 = s"insert into ${tableName} values ('${snapshotLoadTime}', 'W' , 'x', '2017-10-19',2)"
      val insert5 = s"insert into ${tableName} values ( '${snapshotLoadTime}', 'z' , 'z', '2017-10-20',1)"
      val insert6 = s"insert into ${tableName} values ( '${snapshotLoadTime}', 'h' , 'h', '2017-10-20',2)"

      val insert7 = s"insert into ${tableName} values ('${snapshotLoadTime}',  'j' , 'j', '2017-10-20',3)"
      val insert8 = s"insert into ${tableName} values ('${snapshotLoadTime}',  'l' , 'l', '2017-10-20',3)"
      val insert9 = s"insert into ${tableName} values ('${snapshotLoadTime}',  'm' , 'm', '2017-10-20',3)"


      sqlContext.sql(insert1)
      sqlContext.sql(insert2)
      sqlContext.sql(insert3)
      sqlContext.sql(insert4)
      sqlContext.sql(insert5)
      sqlContext.sql(insert6)

      sqlContext.sql(insert7)
      sqlContext.sql(insert8)
      sqlContext.sql(insert9)
    }

    val newIngestion = sqlContext.sql(s"SELECT * FROM ${tableName} ")
    newIngestion.write.partitionBy(partitioningColumns: _*).mode(SaveMode.ErrorIfExists).parquet(loacationDir.getAbsolutePath)
    newIngestion
  }


  private def createAndFillStagingHiveTable(sqlContext: SQLContext, tableName: String, loacationDir: File,
                                            partitioningCriteria: List[String], partitioningTypes: List[String]) = {

    val createTable = s"CREATE TABLE ${tableName} (a string , b string) partitioned by (cob_date string, chunk_id int)"

    sqlContext.sql(createTable)

    val insert1 = s"insert into ${tableName} values ('ra' , 'rb' , '2017-10-18',1)"
    val insert2 = s"insert into ${tableName} values ( 'rd' , 're', '2017-10-18',2)"
    val insert3 = s"insert into ${tableName} values ( 'rx' , 'ry', '2017-10-19',1)"
    val insert4 = s"insert into ${tableName} values ( 'rW' , 'rx', '2017-10-19',2)"
    val insert5 = s"insert into ${tableName} values ( 'rz' , 'rz', '2017-10-20',1)"
    val insert6 = s"insert into ${tableName} values ( 'rh' , 'rh', '2017-10-20',2)"

    sqlContext.sql(insert1)
    sqlContext.sql(insert2)
    sqlContext.sql(insert3)
    sqlContext.sql(insert4)
    sqlContext.sql(insert5)
    sqlContext.sql(insert6)

    val newIngestion = sqlContext.sql(s"SELECT * FROM ${tableName} ")
    newIngestion.write.partitionBy(partitioningCriteria: _*).mode(SaveMode.ErrorIfExists).parquet(loacationDir.getAbsolutePath)
    newIngestion
  }


  private def purge(file: File) = {
    deleteIfExist(file)
    createInDoesntExist(file)

  }

  private def createInDoesntExist(file: File) = {
    if (!file.exists()) {
      file.mkdirs()
    }
  }

  private def deleteIfExist(file: File) = {
    if (file.exists()) {
      file.delete()
    }
  }
}


// TODO
//INSERT OVERWRITE TABLE ... PARTITION ...


// ingestion => staging_temp (warehouse ?) => final (data)

//1. Create staging temp dir ( random)
//2. write partitiond (orginaly) data into staging folder
//3. Write to final folder doing repartitioning with LOAD_TIME as first partition <== LOCK FINAL DIR ?
//4. Get all changed partitions by scanning all subfoldrers in staging (paths)  (Java in
//5. Compare with existing paths in final folder calculating what paths changed ( pathold -> pathnew   snapshot_load_time=xxx/cob_date=yyy/second=zzz => snapshot_load_time=xxx/cob_date=yyy/second=zzz
//6. ALTER TALE LOCATION eplacing old partitions wit new

// TODO IMPALA
//ALTER TABLE table_name PARTITION part_spec SET LOCATION path
//part_spec:
//: (part_col_name1=val1, part_col_name2=val2, ...)


// 7 schedule old partions  in final dir to be deleted (delete stright away?) /// must be locked <== UNLOCK afterwords
//8. Delete staging temp dir

//  import org.apache.hadoop.fs._
//  import org.apache.hadoop.conf._
//  val fs = FileSystem.get(new Configuration())
//  val files = ( fs.listStatus(new Path("C:/MyDocs/2017-09-05/*/*.parquet")) ).map(_.getPath.toString)


//val writer: DataFrameWriter = partitioningCriteria.map(p => df.write.partitionBy(p.split(",").map(_.trim): _*)).getOrElse(df.write)

//val peopleDF = people.toDF("ID","name","age","numFriends")

//val timestamp2datetype: (Column) => Column = (x) => { to_date(x)}